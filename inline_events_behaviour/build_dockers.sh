
#service docker start

REPOSITORY0="events_collector"
if [[ "$(docker images -q $REPOSITORY0 2> /dev/null)" == "" ]]; then
  docker build -t $REPOSITORY0 -f EventsCollector.docker .
fi
REPOSITORY1="events_caller"
if [[ "$(docker images -q $REPOSITORY1 2> /dev/null)" == "" ]]; then
  docker build -t $REPOSITORY1 -f EventsCaller.docker .
fi

#nodejs ./run_experiment.js
