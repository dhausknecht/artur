CREATE DATABASE IF NOT EXISTS inlineevents;

CREATE USER 'inlineevents'@'localhost' IDENTIFIED BY 'inlineevents';

GRANT SELECT,INSERT,CREATE,UPDATE,DELETE ON inlineevents.* TO 'inlineevents'@'localhost';
