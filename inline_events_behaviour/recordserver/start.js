const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const https = require('https');


// get configurations
const configs = JSON.parse(fs.readFileSync(`${__dirname}/../config.json`, 'utf8'));


// create DB connection
let dbConfigs = configs.database;
dbConfigs.tablenames = {};
Object.getOwnPropertyNames(configs.experiments).forEach((name) => {
  dbConfigs.tablenames[name] = configs.experiments[name].db_tablename;
});
const database = require(__dirname+'/db_connection.js')(configs.database);


// init express
let app = express();
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false }));

// view engine setup
app.set('views', `${__dirname}/views`);
app.set('view options', { debug: true });
app.set('view engine', 'pug');
app.locals.pretty = true;


// routings
app.get('/', function (req, res) {
  res.send("just for testing. Up and running!");
});

app.get('/event/:id', function (req, res) {
  // TODO lookup the actual data using :id
  //*
  res.render("eventData", {
    hostname: 'http://test1.tatooine.com/links.php',
    page_url: 'http://test1.tatooine.com/links.php',
    xpath: 'id("myLink")',
    event_name: 'onclick'
  });
  //*/
});

app.post('/events', function (req, res) {
  try {
    /*
    console.log(`[events] ${JSON.stringify(req.body, null, 2)}`);
    res.send("Okay!");
    //*/
    let records = req.body.events.map((event) => {
      event.hostname = req.body.hostname;
      return event;
    });
    return database.recordEventDefinition( records )
            .then(() => { res.send('Okay'); });
  } catch(e) {
    console.log(e);
    res.send("fail!");
  }
});

app.post('/mutations', function (req, res) {
  try {
    console.log(`[mutations] ${JSON.stringify(req.body, null, 2)}`);
    res.send("Okay!");
    /*
    return database.recordEventEffects( req.body )
            .then(() => { res.send('Okay'); });
    */
  } catch(e) {
    console.log(e);
    res.send("fail!");
  }
});

// start server
const options = {
  key: fs.readFileSync(`${__dirname}/${configs.recordserver.key}`),
  cert: fs.readFileSync(`${__dirname}/${configs.recordserver.cert}`)
  //localAddress: "172.17.0.1"
};
var httpsServer = https.createServer(options, app);
httpsServer.listen(configs.recordserver.port);
