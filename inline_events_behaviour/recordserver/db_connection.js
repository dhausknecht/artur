const Sequelize = require('sequelize');
const Url = require('url');


function init(dbConfigs) {
  // open the database connection
  let db_connection = new Sequelize(dbConfigs.name, dbConfigs.username, dbConfigs.password, {
    host: 'localhost',
    dialect: 'mariadb',
    define: {
      timestamps: false // true by default
    },
    logging: false
  });

  // define the model
  let EventDefinition = db_connection.define(dbConfigs.tablenames["events_collector"], {
    "hostname": { type: Sequelize.STRING },
    "page_url": { type: Sequelize.STRING },
    "event_name": { type: Sequelize.STRING },
    "event_code": { type: Sequelize.STRING },
    "element_html": { type: Sequelize.STRING },
    "xpath": { type: Sequelize.STRING }
  });
  /* TODO MutationRecord data
  let EventEffects = db_connection.define(dbConfigs.tablenames["events_caller"], {
    "event_id": { type: Sequelize.INTEGER },
  });
  //*/

  // actually create the model / tables
  db_connection.sync();

  // define functions to handle operations to hide Sequelize
  function recordEventDefinition(records) {
    //console.log(`[recordEventDefinition] ${JSON.stringify(records, null, 2)}`);
    return EventDefinition.bulkCreate( records )
      .then( (created) => true )
      .catch( (reason) =>  {
        console.log(reason);
        return false;
      });
  }
  /* TODO uncomment as soon as EventEffects is defined
  function recordEventEffects(records) {
    return EventDefinition.bulkCreate( records )
      .then( (created) => true )
      .catch( (reason) =>  {
        console.log(reason);
        return false;
      });
  }
  //*/

  // return functions to handle operations
  return {
    recordEventDefinition: recordEventDefinition
    //recordEventEffects: recordEventEffects
  }
}

module.exports = (dbConfigs) => init(dbConfigs);
