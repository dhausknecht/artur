const fs = require('fs');
const execFile = require('child_process').execFile;
const spawn = require('child_process').spawn;
const LineByLineReader = require('line-by-line');


const configs = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
const IP_ADDRESS = require('ip').address();
let currentExperiment = process.argv[2] || "events_caller";


let recordserver = null;


// actually run everything
run();


function run() {
  startUpTools()
    .then(() => visitPages() ) // process all domain names
    .then(() => shutDownTools() )
}


function startUpTools() {
  function startReportServer() {
    return new Promise((resolve, reject) => {
      reportserver = execFile('nodejs', [`${__dirname}/${configs.recordserver.start}`], (error, stdout, stderr) => {
        process.stdout.write(stdout);
        process.stderr.write(stderr);
      });
      reportserver.on('close', (code, signal) => {
        console.log(`record server terminated due to receipt of signal ${signal}`);
      });
      resolve(reportserver);
    });
  }

  return startReportServer()
    .then(() => {
      console.log("record server is up and running... ");
    })
    .catch((err) => {
      console.error("Something went wrong while starting up the record server");
      console.error(err);
    });
}

function visitPages() {
  return new Promise((resolve, reject) => {
    let runningProcesses = [];
    let processed = 0;

    // reading the file starts spawning processes
    let lblr = new LineByLineReader(configs.domainsFile);
    lblr.on('line', (line) => {
      // example: 1, google.com
      line = line.split(",");
      if (line[0] >= configs.startDomain) {
        lblr.pause();
        let domain = line[1];
        startBrowser(domain);
        processed += 1;
        console.log(`[${processed}/${configs.numberOfDomains}] ${domain} (#processes: ${runningProcesses.length})`);
        fillProcessQueue();
      }
    });
    lblr.on('error', function(err){
      console.log('Error while reading file.', err);
      reject();
    });
    lblr.on('end', function () {
      console.log("file is empty"); // All lines are read, file is closed now.
    });

    function fillProcessQueue() {
      if (processed < configs.numberOfDomains && runningProcesses.length < configs.maxNumProcesses) {
        lblr.resume();
      }
    }

    function startBrowser(domain) {
      function onBrowserFinished() {
        runningProcesses.splice(runningProcesses.indexOf(browser),1);
        if (processed < configs.numberOfDomains) {
          fillProcessQueue();
        } else if (runningProcesses.length === 0) {
          console.log("done!");
          resolve();
        }
      }
      // docker run --add-host recordserver.com:192.168.0.6 --name test --rm  -it events_collector bash browse.sh google.se
      let dockerImageName = configs.experiments[currentExperiment].docker.imageName;
      let browser = spawn('docker', ['run','--add-host',`recordserver.com:${IP_ADDRESS}`,'--name',domain,'--rm',dockerImageName,'bash','browse.sh',domain]);
      browser.stdout.on('data', (data) => {
        process.stdout.write(`stdout: ${data}`);
      });
      browser.stderr.on('data', (data) => {
        process.stdout.write(`stderr: ${data}`);
      });
      browser.on('close', (code) => {
        onBrowserFinished();
      });
      runningProcesses.push(browser);

      setTimeout(
        () => {
          let timeout = configs.experiments[currentExperiment].docker.stopTimeout;
          spawn('docker',['stop','--time',timeout,domain]); // 10 is default
        },
        configs.pageLoadingTime
      );
    }
  });
}

function shutDownTools() {
  return new Promise((resolve, reject) => {
    reportserver && reportserver.kill();
    resolve();
  });
}
