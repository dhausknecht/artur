#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#extensiondirs=$(echo /opt/extensions/* | tr " " ",")
extensiondirs=$(echo $DIR/extensions/events_collector | tr " " ",")
#extensiondirs=$(echo $DIR/extensions/events_caller | tr " " ",")

opts=(
	#--proxy-server=0:$proxyport
	--ignore-certificate-errors 			# ignore SSL errors
	--no-sandbox 					# allow running in docker
	--disable-gpu 					# there is no GPU, prevent a crash
	--disable-component-update
	--disable-translate 				# don't use google page translation
	--deterministic-fetch 				# fetch resources in order, remove randomness
	--disable-sync 					# don't sync with google account
	--disable-infobars 				# don't show infobars
	--disable-hang-monitor 				# allow slow renders, don't detect page hangs
	--disable-domain-reliability 			# no domain reliability metrics reporting
	--disable-cloud-import 				# no cloud backup
	--disable-captive-portal-bypass-proxy
	--disable-background-networking 		# avoid network noise
	--disable-prompt-on-repost 			# don't prompt the user on repost
	--no-default-browser-check
	--no-first-run
	--noerrdialogs
	--metrics-recording-only  			# disables metrics reporting
	--safebrowsing-disable-auto-update 		# don't contact safebrowsing auto update URLs
	--load-extension=$extensiondirs
#	--disable-extensions
#	--disable-in-process-stack-traces
#	--dom-automation???
)

	chromium-browser ${opts[*]} "$@" #&> /dev/null
  #chromium-browser ${opts[*]} "$1"
