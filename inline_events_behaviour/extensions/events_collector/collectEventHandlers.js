const EXECUTION_DELAY = 1000 * 30;
const THRESHOLD = 50;

const eventAttributes = ["onabort","onafterprint","onbeforeprint","onbeforeunload",
    "onblur","oncanplay","oncanplaythrough","onchange","onclick","oncontextmenu",
    "oncopy","oncuechange","oncut","ondblclick","ondrag","ondragend","ondragenter",
    "ondragleave","ondragover","ondragstart","ondrop","ondurationchange","onemptied",
    "onended","onerror","onfocus","onhashchange","oninput","oninvalid","onkeydown",
    "onkeypress","onkeyup","onload","onloadeddata","onloadedmetadata","onloadstart",
    "onmousedown","onmousemove","onmouseout","onmouseover","onmouseup","onmousewheel",
    "onoffline","ononline","onpagehide","onpageshow","onpaste","onpause","onplay",
    "onplaying","onpopstate","onprogress","onratechange","onreset","onresize",
    "onscroll","onsearch","onseeked","onseeking","onselect","onshow","onstalled",
    "onstorage","onsubmit","onsuspend","ontimeupdate","ontoggle","onunload",
    "onvolumechange","onwaiting","onwheel"];


function run() {
  function getHTML(elem) {
    return elem.outerHTML.replace(elem.innerHTML,'');
  }

  function sendData(hostname,events) {
    $.ajax({
      method:"POST",
      url: "https://recordserver.com:8888/events",
      contentType:"application/json; charset=utf-8",
      dataType:"json",
      data: JSON.stringify({
        hostname: hostname,
        events:  events
      }),
      success: () => console.log('data sent')
    });
  }

  let eventHandlers = [];
  chrome.runtime.sendMessage({ subject: "getHostname" },
    function(response) {
      let hostname = response.hostname;
      eventAttributes.forEach((event) => {
        $(`[${event}]`).each((index,elem) => {
          let data = {
            page_url: window.location.href,
            event_name: event,
            event_code: elem.getAttribute(event),
            element_html: getHTML(elem),
            xpath: getPathTo(elem)
          };
          //console.log(JSON.stringify( data ),null,2);
          eventHandlers.push( data );
          if (eventHandlers.length > THRESHOLD) {
            sendData(hostname,eventHandlers);
            eventHandlers = [];
          }
        });
      });
      if (eventHandlers.length > 0) {
        sendData(hostname,eventHandlers);
      }
    }
  );
}

// shamelessly borrowed from https://stackoverflow.com/a/2631931
function getPathTo(element) {
  if (element.id!=='')
    return 'id("'+element.id+'")';
  if (element===document.body)
    return element.tagName;

  var ix= 0;
  var siblings= element.parentNode.childNodes;
  for (var i= 0; i<siblings.length; i++) {
    var sibling= siblings[i];
    if (sibling===element)
      return getPathTo(element.parentNode)+'/'+element.tagName+'['+(ix+1)+']';
    if (sibling.nodeType===1 && sibling.tagName===element.tagName)
      ix++;
  }
}

/*
 * Make the code run after everything is loaded
 */
setTimeout(run, EXECUTION_DELAY);
