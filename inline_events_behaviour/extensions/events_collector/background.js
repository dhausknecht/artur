chrome.webRequest.onHeadersReceived.addListener(function(details) {
    if (details && details.responseHeaders) {
      details.responseHeaders = details.responseHeaders.filter(function(header) {
        // if is NOT CSP header
        return (! header.name.match(/content-security-policy|^x-.*-csp(-report-only)?$/i));
      });
      return {responseHeaders: details.responseHeaders};
    }
  },
  {"urls": ["<all_urls>"]},
  ["blocking", "responseHeaders"]
);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.subject === "getHostname") {
      sendResponse({ hostname: new URL(sender.tab.url).hostname });
    }
  }
);
