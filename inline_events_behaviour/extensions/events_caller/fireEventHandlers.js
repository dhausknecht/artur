const EXECUTION_DELAY = 1000 * 2;

/*
1) start browser with `https://recordserver.com:8888/event/${EVENT_ID}`
2) server responses with HTML containing all important info
content script A...
3) loads it into extension
4) redirects to hostname
content script B...
5) gets info from extension
6) finds element through xpath
7) registers MutationObserver
7) fires event on element
8) reports all mutations
*/

let eventData = {};
chrome.runtime.sendMessage({ subject: "getEventData" },
  function(response) {
    // gets info from extension
    eventData = {
      hostname: response.hostname,
      page_url: response.page_url,
      xpath: response.xpath,
      event_name: response.event_name
    };
    console.log(JSON.stringify(eventData, null, 2));
    /*
     * Make the code run after everything is loaded
     */
    setTimeout(run, EXECUTION_DELAY);
  }
);

function findElementByXPath(xpath) {
  let xpath_list = eventData.xpath.split("/");
  console.log(xpath_list);
  let elem = xpath_list.reduce(
    (e,value) => {
      switch (true) {
        // something went wrong before
        case null:
          return null;
        // id("myId")
        case /id\(".*"\)/i.test(value):
          let id = /id\("(.*)"\)/i.exec(value)[1];
          return document.getElementById(id);
        case /body/i.test(value):
          return document.body;
        case /\s+[\d+]/i.test(value):
          let p = /(\s+)[(\d+)]/i.exec(value);
           let elems = e.getElementsByTagName(p[1]);
           if (elems.length >= p[2]) {
             return elems[ p[2] ];
           }
           return null;
        default:
          return null;
      }
    },
    document
  );
  return elem;
}

function run() {
  // finds element through xpath
  let elem = findElementByXPath(eventData.xpath);
  console.log(elem);

  // TODO reports all mutations
  let reportFun = (mutations) => {
    mutations.forEach((m) => console.log(
      JSON.stringify({
        type:               m.type,
        target:             getHTML(m.target),
        addedNodes:         getHTMLlist(m.addedNodes),
        removedNodes:       getHTMLlist(m.removedNodes),
        previousSibling:    m.previousSibling,
        nextSibling:        m.nextSibling,
        attributeName:      m.attributeName,
        attributeNamespace: m.attributeNamespace,
        oldValue:           m.oldValue
      })
    ));
    /* TODO send it out to the server
    $.post("https://recordserver.com:8888/events", {
      hostname: window.location.hostname,
      events: JSON.stringify( eventHandlers )
    });
    //*/
  }
  // registers MutationObserver
  // TODO let mo = registerMutationObserver(reportFun);
  // TODO fires event on element
}

function getHTML(elem) {
  return elem.outerHTML.replace(elem.innerHTML,'');
}

function getHTMLlist(nodeList) {
  return [...nodeList].map((elem) =>  getHTML(elem));
}

function registerMutationObserver(reportFun) {
  let mo = new MutationObserver(reportFun);
  mo.observe(
    document, // Node target
    {       // MutationObserverInit options
      childList: true,
      attributes: true,
      characterData: true,
      subtree: true,
      attributeOldValue: true,
      characterDataOldValue: true
      // attributeFilter
    }
  );
  return mo;
}
