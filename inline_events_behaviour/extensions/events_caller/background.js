chrome.webRequest.onHeadersReceived.addListener(function(details) {
    if (details && details.responseHeaders) {
      details.responseHeaders = details.responseHeaders.filter(function(header) {
        // if is NOT CSP header
        return (! header.name.match(/content-security-policy|^x-.*-csp(-report-only)?$/i));
      });
      return {responseHeaders: details.responseHeaders};
    }
  },
  {"urls": ["<all_urls>"]},
  ["blocking", "responseHeaders"]
);


let eventData = {};
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    switch (request.subject) {
      case "eventData":
        eventData = {
          hostname: request.hostname,
          page_url: request.page_url,
          xpath: request.xpath,
          event_name: request.event_name
        };
        sendResponse({ location: eventData.hostname });
        break;
      case "getEventData":
        sendResponse(eventData);
        break;
      default:
        console.error(`unknown request.subject '${request.subject}'`);
    }
  }
);
