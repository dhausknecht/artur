let msg = { subject: "eventData" };
msg.hostname = document.getElementById("hostname").textContent;
msg.page_url = document.getElementById("page_url").textContent;
msg.xpath = document.getElementById("xpath").textContent;
msg.event_name = document.getElementById("event_name").textContent;

chrome.runtime.sendMessage(
  msg,
  function(response) {
    window.location.href = response.location;
  }
);
