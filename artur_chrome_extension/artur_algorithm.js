/*
 * This implementation follows the ideas and algorithms implemented in
 *     https://mikewest.github.io/artur-yes/
 *
 * Example policy:
 * XSS-Protection: {
 *    "nonce": "RANDOM==",
 *    "hash": ["sha256-asdf", "sha256-qwer"],
 *    "reflection": "block",  // <- can't do that in an extension
 *    "report": "group1"
 * }
 */

var HEADER_NAME = "XSS-Protection".toLowerCase();

chrome.webRequest.onHeadersReceived.addListener(
  function(details) {
    var policies_list = [];
    details.responseHeaders.forEach((header, i) => {
      if (header.name.toLowerCase() === HEADER_NAME) {
        policies_list = compileArtur(policies_list,header.value);
      }
    });
    policies_list.forEach((policy) => {
      details.responseHeaders.push(policy.toHeader());
    });
    return {responseHeaders: details.responseHeaders};
  },
  {urls: ["<all_urls>"]},
  ["blocking", "responseHeaders"]
);

function compileArtur(policies_list, artur_header_value_str) {
  var xss_policy = {};
  try {
    xss_policy = JSON.parse(artur_header_value_str);
  } catch(error) {
    console.error(error.message);
    return policies_list;
  }

  // code safety: check if header value is object
  // TODO more structural checks as in Mike's draft 2.2.1.
  //      But it's just a prototype, so meh ;-)
  if (! isObject(xss_policy)) {
    return policies_list;
  }

  // 4.1.1 in Mike's draft
  var csp_policy = new Policy("enforce");
  csp_policy.setDirective("script-src", "'strict-dynamic'");
  csp_policy.setDirective("object-src", "'none'");
  xss_policy && Object.keys(xss_policy).forEach((key) => {
    switch (key) {
    case "nonce":
      var nonce = xss_policy[key];
      if (isValidBase64Value(nonce)) {
        csp_policy.appendToDirective("script-src", `'nonce-${nonce}'`);
      } else {
        console.log(`What the... Your nonce '${nonce}' is not even a base64 value!!!`);
      }
      break;
    case "hash":
      xss_policy[key].isArray() && xss_policy[key].forEach((hash_value) => {
        csp_policy.appendToDirective("script-src", "'"+hash_value+"'");
      });
      break;
    case "reflection":
      console.log("I cannot enforce 'reflection' as an extension."
                  +"I will ignore it now. Sorry...");
      break;
    default:
      console.log(`Seriously? What the **** is '${key}' supposed to be?`);
    }
  });
  policies_list.push(policy);
  console.log(JSON.stringify(policy.toHeader(), null, 2));

  return policies_list; // or true? Not that I care right now...
}


function isArray(obj) {
  return obj && obj.constructor === Array;
}
function isObject(obj) {
  return obj && obj.constructor === Object;
}


function isValidBase64Value(value) {
  var nonce_re = new RegExp("(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?");
  return (typeof value === 'string' || value instanceof String)
          && nonce_re.test(value);
}

function isValidToken(token) {
  // TODO
  return true;
}


function Policy(disposition_str) {
  var mDisposition = disposition_str;
  var mDirectives = {};

  this.getDisposition = () => {
    return mDisposition
  };

  this.getDirective = (dir_str) => {
    return mDirectives[dir_str];
  };

  this.setDirective = (dir_str,value) => {
    mDirectives[dir_str] = value;
    return this;
  };

  this.appendToDirective = (dir_str,value) => {
    if (!mDirectives[dir_str]) {
      return this.setDirective(dir_str,value);
    } else {
      mDirectives[dir_str] += " "+value;
      return this;
    }
  };

  this.toHeader = () => {
    var name_str = "Content-Security-Policy"
                    +((mDisposition === "enforce") ? "" : "-Report-Only");

    var value_str = "";
    Object.keys(mDirectives).forEach((dir) => {
      value_str += dir + " " + mDirectives[dir] + "; ";
    });

    return { name: name_str, value: value_str };
  }
}

chrome.webRequest.onBeforeSendHeaders.addListener(
  function(details) {
    details.requestHeaders.push({
      name:  "xss-protection-extension",
      value: "1"
    });
    return {requestHeaders: details.requestHeaders};
  },
  {urls: ["<all_urls>"]},
  ["blocking", "requestHeaders"]
);
