const Sequelize = require('sequelize');
const Url = require('url');


// configurations
let dbname = "csp_reports";
let username = "reportserver";
let password = "cspreportserver";


function init(tablename) {
  // open the database connection
  let db_connection = new Sequelize(dbname, username, password, {
    host: 'localhost',
    dialect: 'mariadb',
    define: {
      timestamps: false // true by default
    },
    logging: false
  });

  // define the model
  let ChromeReport = db_connection.define(tablename, {
    "hostname": { type: Sequelize.STRING },
    "document_uri": { type: Sequelize.STRING },
    "referrer": { type: Sequelize.STRING },
    "violated_directive": { type: Sequelize.STRING },
    "effective_directive": { type: Sequelize.STRING },
    "original_policy": { type: Sequelize.STRING },
    "disposition": { type: Sequelize.STRING },
    "blocked_uri": { type: Sequelize.STRING },
    "line_number": { type: Sequelize.STRING },
    "status_code": { type: Sequelize.STRING },
    "column_number": { type: Sequelize.STRING },
    "source_file": { type: Sequelize.STRING }
  });

  // actually create the model / tables
  db_connection.sync();

  // define functions to handle operations to hide Sequelize
  function recordReport(report) {
    report = JSON.parse(report)["csp-report"];
    report = Object.getOwnPropertyNames(report).reduce((rep, pname) => {
      rep[pname.replace(/-/g,"_")] = report[pname];
      return rep;
    }, {
      hostname: Url.parse(report["document-uri"]).hostname
    });
    return ChromeReport.create( report )
      .then( (createdReport) => true )
      .catch( (reason) =>  {
        console.log(reason);
        return false;
      });
  }

  // return functions to handle operations
  return {
    recordReport: recordReport
  }
}

module.exports = (tablename) => init(tablename);
