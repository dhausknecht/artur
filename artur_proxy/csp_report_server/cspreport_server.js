const http = require('http');
const https = require('https');
const fs = require('fs');


// 0: node, 1: path/to/script.js, 2: first arg
if (process.argv[2]) {
  let tablename = process.argv[2].replace(/"/g,""); // quotes might be in there from jq
  const database = require(__dirname+'/db_connection.js')(tablename);

  const options = {
    key: fs.readFileSync(__dirname+'/keys/localhost.key'),
    cert: fs.readFileSync(__dirname+'/keys/localhost.crt'),
    localAddress: "172.17.0.1"
  };

  //const hostname = 'cspreportserver.com';
  const port = 8888;

  const server = https.createServer(options, (req, res) => {
    //const server = http.createServer((req, res) => {
    var body = [];
    req.on('data', function(chunk) {
      body.push(chunk);
    }).on('end', function() {
      body = Buffer.concat(body).toString();
      //console.log(JSON.stringify(JSON.parse(body),null,2)+",");
      return database.recordReport(body)
        .then(() => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'text/plain');
          res.end('Okay');
        });
    });
  });

  //server.listen(port, hostname, () => {
  server.listen(port, () => {
    //console.log(`Server running at https://${hostname}:${port}/`);
  });

} else {
  console.log("Usage: nodejs cspreport_server.js <table name>");
  process.exit();
}
