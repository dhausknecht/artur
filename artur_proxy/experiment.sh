
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DOMAINS_FILE="$SCRIPTDIR/top-1m.csv"
#DOMAINS_FILE="$SCRIPTDIR/facebook.txt"
#DOMAINS_FILE="$SCRIPTDIR/test_domains.txt"
MAX_NUM_PAGES=3     # max number of concurrently open pages
PAGE_LOAD_TIME=25   # time the browser stays on a single page
MAX_DOMAINS=200     # total max number of processed pages

browser_cmd=""
function generateBrowserCmd {
  domains=$@
  proxyport=8080
  opts=(
    --disk-cache-dir=/dev/null
  	--proxy-server=0:$proxyport
  	--ignore-certificate-errors 			# ignore SSL errors
  	--no-sandbox 					# allow running in docker
  	--disable-gpu 					# there is no GPU, prevent a crash
  	--disable-component-update
  	--disable-translate 				# don't use google page translation
  	--deterministic-fetch 				# fetch resources in order, remove randomness
  	--disable-sync 					# don't sync with google account
  	--disable-infobars 				# don't show infobars
  	--disable-hang-monitor 				# allow slow renders, don't detect page hangs
  	--disable-domain-reliability 			# no domain reliability metrics reporting
  	--disable-cloud-import 				# no cloud backup
  	--disable-captive-portal-bypass-proxy
  	--disable-background-networking 		# avoid network noise
  	--disable-prompt-on-repost 			# don't prompt the user on repost
  	--no-default-browser-check
  	--no-first-run
  	--noerrdialogs
  	--metrics-recording-only  			# disables metrics reporting
  	--safebrowsing-disable-auto-update 		# don't contact safebrowsing auto update URLs
  #	--load-extension=$extensiondirs
  	--disable-extensions
  #	--disable-in-process-stack-traces
  #	--dom-automation???
  )

  browser_cmd="xvfb-run chromium-browser ${opts[*]} $domains"
}


# start up all proxy and CSP report server
proxy=""
reportserver=""
function startUpTools {
  mitmdump --insecure --anticache -q -s "artur_mitmproxy.py --policyname $POLICY_NAME" &> proxy.out &
  proxy=$!
  echo "Proxy started (pid: $proxy)"
  cd "$SCRIPTDIR/csp_report_server"
  nodejs cspreport_server.js $POLICY_NAME &> ../reportserver.out &
  reportserver=$!
  cd "$SCRIPTDIR"
  echo "CSP report server started (pid: $reportserver)"
  sleep 2
}

# start the browser and visit all domains
domains=()
function visitPages {
  generateBrowserCmd ${domains[@]}
  startUpTools
  timeout $PAGE_LOAD_TIME $browser_cmd  &> /dev/null
  shutDownTools
  domains=()
}

# shut down proxy and CSP report server
function shutDownTools {
  kill $proxy
  echo "Proxy stopped ($proxy)."
  kill $reportserver
  echo "CSP report server stopped ($reportserver)"
}


# crawl the web
function crawl {
  PROCESSED=0
  while [ $PROCESSED -lt $MAX_DOMAINS ] && IFS=',' read -ra LINE
  do
    PROCESSED=$((PROCESSED+1))
    echo "[$PROCESSED/$MAX_DOMAINS] ${LINE[0]} - ${LINE[1]}"
    domains+=(${LINE[1]})
    if [ "${#domains[@]}" -ge "$MAX_NUM_PAGES" ]
    then
      visitPages
    fi
  done < $DOMAINS_FILE
  if [ "${#domains[@]}" -gt "0" ]
  then
    visitPages
  fi
}

POLICY_NAME=""
while read TABLENAME; do
  POLICY_NAME=$TABLENAME
  crawl
done < <(jq '.[].tablename' "$SCRIPTDIR/config.json")
