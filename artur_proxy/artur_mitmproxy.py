
import re,string,random,json,os,argparse
from bs4 import BeautifulSoup
from functools import reduce


CONFIG_FILE = os.path.dirname(os.path.realpath(__file__))+"/config.json"
CONTENT_TYPE_HTML_RE = re.compile("html",re.I)
NONCES_RE = re.compile("nonce-(\w+=*)")
REPORT_URI = "https://cspreportserver.com:8888/"
CSP_HEADER_NAME = "Content-Security-Policy-Report-Only";


def start():
    with open(CONFIG_FILE) as json_file:
        configs = json.load(json_file)
    parser = argparse.ArgumentParser()
    parser.add_argument("--policyname", type=str)
    policyname = parser.parse_args().policyname
    for config in configs:
      if config['tablename'] == policyname:
        print("Starting proxy to inject the following policy ('{0}'):\n\t{1}".format(policyname,config['csp_policy']))
        return CSPInjector( config['csp_policy'] );
    print("Configurarion '{0}' not found. Please check your config file '{1}'".format(policyname,CONFIG_FILE))


class CSPInjector:
    def __init__(self,csp_policy):
        self.csp_policy = csp_policy

    def generate_ARTUR(self,nonces, report_uri):
      nonces = nonces if isinstance(nonces,list) else [nonces]
      return self.csp_policy.format(
                " ".join(["'nonce-{}'".format(n) for n in nonces]),
                str(report_uri)
              );


    def is_HTML_response(self,flow):
      try:
        if (len(flow.response.content) > 0 and flow.response.status_code == 200
            and len([h for h in flow.response.headers if h.lower() == "location"]) == 0
            and len([h for h in flow.response.headers if h.lower() == "content-type"]) > 0):
          contentType = flow.response.headers["content-type"]
          return ( contentType is not None and len(contentType) > 0
                  and CONTENT_TYPE_HTML_RE.search(contentType) is not None)
        return False
      except:
        return False


    def generate_nonce(self,size=16, chars=string.ascii_lowercase+string.ascii_uppercase+string.digits):
      #return ''.join(random.choice(chars) for x in range(size))
      return "SUPERSECRETNONCE"


    def add_ARTUR_header(self,headers):
      artur_nonce = None
      # get existing CSP headers
      CSP_headers = headers.get_all("Content-Security-Policy")
      if len(CSP_headers) == 0:
        # inject completely new ARTUR with new nonce
        artur_nonce = self.generate_nonce()
        headers[CSP_HEADER_NAME] = self.generate_ARTUR(artur_nonce,REPORT_URI)
      else:
        # reuse existing CSP for ARTUR
        nonces = reduce(lambda res,csp: res+NONCES_RE.findall(csp), CSP_headers, [])
        print("Nonces: "+str(nonces))
        # has nonce?
        if len(nonces) > 0:
          # yes: inject completely new parallel ARTUR re-using nonce
          artur_nonce = nonces[0]
          artur_policy = self.generate_ARTUR([n for n in set(nonces)],REPORT_URI)
          headers.set_all(CSP_HEADER_NAME, CSP_headers+[artur_policy])
        else:
          # no: inject completely new parallel ARTUR with new nonce
          artur_nonce = self.generate_nonce()
          artur_policy = self.generate_ARTUR(artur_nonce,REPORT_URI)
          headers.set_all(CSP_HEADER_NAME, CSP_headers+[artur_policy])
      return artur_nonce


    def inject_nonce(self,nonce,htmlString):
      soup = BeautifulSoup(htmlString, 'html.parser')
      for scriptTag in soup.find_all("script"):
        if "nonce" in scriptTag.attrs:
          print('scriptTag["nonce"]: '+str(scriptTag["nonce"]))
        else:
          scriptTag["nonce"] = "{}".format(nonce)
      return soup.prettify().encode("utf8")


    def response(self,flow):
      if (self.is_HTML_response(flow)):
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print(str(flow.request.method)+" ("+str(flow.response.status_code)+") - "+str(flow.request.url)+", length: "+str(len(flow.response.content)))
        print("content-type: "+flow.response.headers["content-type"])
        #print("content-encoding: "+flow.response.headers["content-encoding"]) # might not exist -> crashes
        flow.response.decode()
        artur_nonce = self.add_ARTUR_header(flow.response.headers)
        print("ARTUR Nonce: "+artur_nonce)
        html = self.inject_nonce(artur_nonce,flow.response.content)
        flow.response.content = html
        print("content after:")
        print(flow.response.content[0:256])
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
