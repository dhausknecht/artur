const fs = require('fs');
const execFile = require('child_process').execFile;
const spawn = require('child_process').spawn;
const LineByLineReader = require('line-by-line');


const CONFIG_FILE = `${__dirname}/config.json`;
const DOMAINS_FILE = `${__dirname}/top-1m.csv`;
const REPORT_SERVER_SCRIPT=`${__dirname}/csp_report_server/cspreport_server.js`;
const IP_ADDRESS = '192.168.0.6';
const MAX_PROCESSES = 4;
const MAX_NUM_PAGES = 500;
const PAGE_LOAD_TIME = 1000*25;
const DOCKER_STOP_TIMEOUT = 0;


let reportserver = null;


run();

function run() {
  // { "tablename": <string>, "csp_policy": <string> }
  let configs = JSON.parse(fs.readFileSync(CONFIG_FILE, 'utf8'));

  // for each tablename in config
  function processConfigs(configs) {
    if (configs.length > 0) {
      let config = configs.shift();
      startUpTools(config.tablename)
        .then(() => visitPages(config.tablename) ) // process all domain names
        .then(() => shutDownTools() )
        .then(() => processConfigs(configs));
    } else {
      // TODO resolve();
    }
  }
  processConfigs(configs);
}


function startUpTools(policyname) {
  function startReportServer(policyname) {
    return new Promise((resolve, reject) => {
      reportserver = execFile('nodejs', [REPORT_SERVER_SCRIPT,policyname], (error, stdout, stderr) => {
        process.stdout.write(stdout);
        process.stderr.write(stderr);
      });
      reportserver.on('close', (code, signal) => {
        console.log(`report server terminated due to receipt of signal ${signal}`);
      });
      resolve(reportserver);
    });
  }

  return startReportServer(policyname)
    .then(() => {
      console.log("report server is up and running... "+policyname);
    })
    .catch((err) => {
      console.error("Something went wrong while starting up the report server");
      console.error(err);
    });
}

function visitPages(policyname) {
  return new Promise((resolve, reject) => {
    let runningProcesses = [];
    let processed = 0;

    // reading the file starts spawning processes
    let lblr = new LineByLineReader(DOMAINS_FILE);
    lblr.on('line', (line) => {
      lblr.pause();
      let domain = line.split(",")[1];
      startBrowser(domain);
      processed += 1;
      console.log(`[${processed}/${MAX_NUM_PAGES}] ${domain} (#processes: ${runningProcesses.length})`);
      fillProcessQueue();
    });
    lblr.on('error', function(err){
      console.log('Error while reading file.', err);
      reject();
    });
    lblr.on('end', function () {
      console.log("file is empty"); // All lines are read, file is closed now.
    });

    function fillProcessQueue() {
      if (processed < MAX_NUM_PAGES && runningProcesses.length < MAX_PROCESSES) {
        lblr.resume();
      }
    }

    function startBrowser(domain) {
      function onBrowserFinished() {
        runningProcesses.splice(runningProcesses.indexOf(browser),1);
        if (processed < MAX_NUM_PAGES) {
          fillProcessQueue();
        } else if (runningProcesses.length === 0) {
          console.log("done!");
          resolve();
        }
      }
      // docker run --add-host cspreportserver.com:192.168.0.6 --name test --rm  -it artur_experiment bash browse.sh none_reports youtube.com
      let browser = spawn('docker', ['run','--add-host',`cspreportserver.com:${IP_ADDRESS}`,'--name',domain,'--rm','artur_experiment','bash','browse.sh',policyname,domain]);
      browser.stdout.on('data', (data) => {
        process.stdout.write(`stdout: ${data}`);
      });
      browser.stderr.on('data', (data) => {
        process.stdout.write(`stderr: ${data}`);
      });
      browser.on('close', (code) => {
        onBrowserFinished();
      });
      runningProcesses.push(browser);

      setTimeout(
        () => {
          spawn('docker',['stop','--time',DOCKER_STOP_TIMEOUT,domain]); // 10 is default
        },
        PAGE_LOAD_TIME
      );
    }
  });
}

function shutDownTools() {
  return new Promise((resolve, reject) => {
    reportserver && reportserver.kill();
    resolve();
  });
}
