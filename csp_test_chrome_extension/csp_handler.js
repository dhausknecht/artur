function getWannaHaveCSP() {
  return {
    "script-src": ["'strict-dynamic'", "'unsafe-inline'", "'unsafe-eval'"], // "'nonce-" missing, get from original CPS
    "object-src": ["'none'"],
    //"base-uri": ["'none'"],
    "sandbox": ["allow-forms", "allow-modals", "allow-orientation-lock",
                "allow-pointer-lock", "allow-popups", "allow-popups-to-escape-sandbox",
                "allow-presentation", "allow-scripts", "allow-top-navigation"
                ,"allow-same-origin"
                ]
  };
  //"report-uri": "https://csp.example.com"
}

var mainFrameCSP = null;

chrome.webRequest.onHeadersReceived.addListener(
  function(details) {
    details.responseHeaders.forEach(function(header) {
      if (header.name.match(/content-security-policy|^x-.*-csp(-report-only)?$/i)
          && (details.type.match(/.*frame$/i))) {
        let cspObj = cspToObj(header.value);

        console.log(
           `header: ${header.name}\n`
          +`type: ${details.type}, frameId: ${details.frameId}\n`
          +`value: ${JSON.stringify(cspObj,null,2)}\n`
        );

        let newCSP = getWannaHaveCSP();
        if (cspObj["base-uri"]) {
          newCSP["base-uri"] = [ cspObj["base-uri"] ];
        }
        cspObj["script-src"]
          .filter( (val) => /nonce-.*/.test(val) )
          .forEach((nonce) => { newCSP["script-src"].push(nonce) });
        newCSP = objToCSP(newCSP);

        if (details.type.match(/main_frame$/i)) {
          mainFrameCSP = newCSP;
        } else if (details.type.match(/sub_frame$/i)
            && mainFrameCSP && mainFrameCSP["script-src"]) {
          mainFrameCSP["script-src"]
            .filter( (val) => !(/nonce-.*/.test(val)) )
            .concat( cspObj["script-src"].filter( (val) => /nonce-.*/.test(val) ) );
          details.responseHeaders.push({
            name: "content-security-policy",
            value: objToCSP(mainFrameCSP)
          });
        }
        newCSP = objToCSP(newCSP);

        console.log(newCSP);

        header.value = newCSP;
      }
    });
    return { responseHeaders: details.responseHeaders };
  },
  {urls: ["<all_urls>"]},
  ["blocking", "responseHeaders"]
);


function cspToObj(csp_str) {
  cspObj = {};
  csp_str = csp_str
    .split(/\s*;\s*/)
    .forEach((dir) => {
      let a = dir.split(/\s+/);
      let key = a.shift();
      cspObj[key] = a;
    });
  return cspObj;
}

function objToCSP(cspObj) {
  return Object.getOwnPropertyNames(cspObj)
    .map((key) => `${key} ${cspObj[key].join(" ")};`)
    .join(" ");
}
