# Abstract #

If Artur is yes, then nonce is go!

# Extension #
The extension modifies requests and responses to make Chrome ARTUR HTTP header aware.

* **Request modifications:**
  Simply the "artur-extension: 1" HTTP header is added to tell the server 
  the extension is installed. This results in a different response (a CSP 
  "default-src 'none'").

* **Response modifications:**
  Any "Artur" HTTP response header is compiled into a corresponding CSP 
  header which is then added to the response. Nothing is removed or modified.

That's it, no real magic here.

Check out the ARTUR test page on [https://danielhausknecht.eu/artur](https://danielhausknecht.eu/artur).